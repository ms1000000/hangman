# Hangman

A Hangman game for the Windows Console.

### [Click here](https://gitlab.com/ms1000000/hangman/-/releases/v1.0.1) to download the latest stable release (v1.0.1).

#### Found a bug? Report it [right here](https://gitlab.com/ms1000000/hangman/-/issues).

#### Want to contribute some code to the project, for example to solve a bug that was reported, or one that you found by yourself? You can do so [right here](https://gitlab.com/ms1000000/hangman/-/merge_requests).
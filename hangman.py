# Import modules
import msvcrt
import os
import random
import ctypes

# Declaring global variables

# Defines what a regular character is
regular_characters = ""

# Present all the word options and (in next versions) randomly generate an index of a word
words = []
word_index = 0

# Whether the user guessed the word (0: Didn't guess yet, 1: Ran out of wrong guesses, 2: Guessed the wrong word,
# 3: Guessed all the letters, 4: Guessed the right word)
guessed = 0

# String displayed to the user
under_string = ""

# Statistics for statistics bar
wrong_guesses_left = 0
used_letters = []


# Transform under_string
def t_under_string():
    under_string_withcaps = under_string[0].upper() + under_string[1:len(under_string)]
    transformed_string = ""

    i = 0
    while i < len(words[word_index]):
        transformed_string += under_string_withcaps[i] + " "

        i += 1

    transformed_string = transformed_string[0:len(transformed_string) - 1]
    return transformed_string


# Create stats text
def stats_text():
    text = "\u001b[33m" + "Wrong guesses left: " + str(wrong_guesses_left) + "."
    if len(used_letters) != 0:
        text += " Used letters: "

        i = 0
        while i < len(used_letters):
            text += used_letters[i] + ", "

            i += 1

        text = text[0:len(text) - 2] + "."

    text += "\u001b[0m"

    return text


# Check for irregular characters
def check_irreg_chars(string):
    is_irreg = False

    input_index = 0
    while input_index < len(string) and is_irreg == 0:
        matched = False

        char_index = 0
        while char_index < len(regular_characters) and not matched:
            if regular_characters[char_index] == string[input_index]:
                matched = True

            char_index += 1

        if matched == 0:
            is_irreg = True

        input_index += 1

    return is_irreg


# Main function
def main():
    global regular_characters, words, word_index, under_string, wrong_guesses_left, used_letters, guessed

    # Make Windows think the program works in a command line
    os.system("")

    # Defining global variables
    regular_characters = "qwertyuiopasdfghjklzxcvbnm"
    wrong_guesses_left = 6

    words_file = open("words.txt", "r+")
    words_file_string = words_file.read()
    words = words_file_string.split("\n")
    words_file.close()
    del words_file, words_file_string
    
    word_index = random.randint(0, len(words) - 1)
    under_string = "_" * len(words[word_index])

    ctypes.windll.kernel32.SetConsoleTitleW("Hangman")

    # This code will be executed each turn
    while guessed == 0:
        user_input = ""
        first_time = True

        while len(user_input) != 1 and len(user_input) != len(words[word_index]) or check_irreg_chars(user_input) or user_input in used_letters:
            error = ""
            if user_input != "":
                if len(user_input) != 1 and len(user_input) != len(words[word_index]) and not first_time:
                    error = "\u001b[31;1m" + "ERROR: PLEASE TYPE A LETTER OR A WORD WITH THE SAME LENGTH AS THE WORD YOU'RE SUPPOSED TO GUESS HAS (AS SHOWN ON SCREEN). " + "\u001b[0m" + "\n"
                elif check_irreg_chars(user_input):
                    error = "\u001b[31;1m" + "ERROR: PLEASE TYPE ONLY ENGLISH LETTERS." + "\u001b[0m" + "\n"
                elif not first_time:
                    error = "\u001b[31;1m" + "ERROR: YOU ALREADY ENTERED THIS LETTER." + "\u001b[0m" + "\n"

            print(t_under_string() + "\n\n" + stats_text() + "\n\n" + error + "Please type a letter or a word: ", end="")
            user_input = input().lower()

            first_time = False

            print("\u001b[2J" + "\u001b[1;1H", end="")

        if len(user_input) == 1:
            new_under_string = ""

            i = 0
            while i < len(under_string):
                if words[word_index][i] == user_input:
                    new_under_string += user_input
                else:
                    new_under_string += under_string[i]

                if user_input not in used_letters:
                    used_letters += user_input

                i += 1

            if new_under_string == under_string:
                wrong_guesses_left -= 1

            under_string = new_under_string

        else:
            if user_input == words[word_index]:
                guessed = 4
            else:
                guessed = 2

        if under_string == words[word_index]:
            guessed = 3
        elif wrong_guesses_left == 0:
            guessed = 1

    if guessed == 3:
        print("Good job! You guessed all the letters!\nThe word was:\n" + words[word_index].title())
    elif guessed == 1:
        print("Oh no! You ran out of guesses! Maybe next time you'll succeed...\nThe word was:\n" + words[word_index].title())
    elif guessed == 4:
        print("Good job! You guessed the right word!\nThe word was:\n" + words[word_index].title())
    else:
        print("Oh no! You guessed the wrong word! Maybe next time you'll succeed...\nThe word was:\n" + words[word_index].title())

    msvcrt.getch()


if __name__ == "__main__":
    main()
